function fetch_next_contest_id() {
  const req = new XMLHttpRequest();
  var res = null;
  req.onreadystatechange = function() {		  // XMLHttpRequest オブジェクトの状態が変化した際に呼び出されるイベントハンドラ
    if(req.readyState == 4 && req.status == 200){ // サーバーからのレスポンスが完了し、かつ、通信が正常に終了した場合
      const json = JSON.parse(req.responseText);

      const now_epoch_sec = (new Date()).getTime();
      epoch_sec_dist = 4611686018427387904;
      ind = -1;

      for (let i=0; i<json.length; i++) {
        if (Math.abs(json[i]['start_epoch_second'] - now_epoch_sec) < epoch_sec_dist) {
          ind = i;
          epoch_sec_dist = Math.abs(json[i]['start_epoch_second'] - now_epoch_sec);
        }
      }

      res = json[ind]['id'];
    }
  };
  req.open("GET", "https://kenkoooo.com/atcoder/resources/contests.json", false);
  req.send(null);

  return res;
}


function onTabUpdate(tabId, changeInfo, tab) {
  chrome.tabs.query({ active: true, currentWindow: true }, (e) => {
    const url = e[0].url;
    if (url == 'https://atcoder.jp/contests/latest') {
      const contest_id = fetch_next_contest_id();
      chrome.tabs.update(tabId, { url:  'https://atcoder.jp/contests/' + contest_id});
    }
  });
}


chrome.tabs.onUpdated.addListener(onTabUpdate);
